#include <math.h>
#include <stdlib.h>
#include "stm32f1xx.h"
#include "sun_calculations.h"

/*calculates day of year number based on current date*/
int day_of_year(RTC_DateTypeDef *date){

	/*find whether the year is leap or not, standard algorithm*/
	int day_number = 0;
	int is_leap = 0;
	int curr_year = date->Year + 2000;

	if(curr_year % 4 != 0){
		is_leap = 0;
	}
	else if(curr_year % 100 != 0){
		is_leap = 1;
	}
	else if(curr_year % 400 != 0){
		is_leap = 0;
	}
	else{
		is_leap = 1;
	}

	/*find number of the day in the year*/
	switch(date->Month){
	case RTC_MONTH_JANUARY:
		day_number = date->Date;
		break;
	case RTC_MONTH_FEBRUARY:
		day_number = 31 + date->Date;
		break;
	case RTC_MONTH_MARCH:
		day_number = (is_leap ? 60 + date->Date : 59 + date->Date);
		break;
	case RTC_MONTH_APRIL:
		day_number = (is_leap ? 91 + date->Date : 90 + date->Date);
		break;
	case RTC_MONTH_MAY:
		day_number = (is_leap ? 121 + date->Date : 120 + date->Date);
		break;
	case RTC_MONTH_JUNE:
		day_number = (is_leap ? 152 + date->Date : 151 + date->Date);
		break;
	case RTC_MONTH_JULY:
		day_number = (is_leap ? 182 + date->Date : 181 + date->Date);
		break;
	case RTC_MONTH_AUGUST:
		day_number = (is_leap ? 213 + date->Date : 212 + date->Date);
		break;
	case RTC_MONTH_SEPTEMBER:
		day_number = (is_leap ? 244 + date->Date : 243 + date->Date);
		break;
	case RTC_MONTH_OCTOBER:
		day_number = (is_leap ? 274 + date->Date : 273 + date->Date);
		break;
	case RTC_MONTH_NOVEMBER:
		day_number = (is_leap ? 305 + date->Date : 304 + date->Date);
		break;
	case RTC_MONTH_DECEMBER:
		day_number = (is_leap ? 335 + date->Date : 334 + date->Date);
		break;

	}
	return day_number;
}


/*calculates the declination angle for a given day of year*/
float calc_declination_deg(int day_number){

	float declination = -23.45*cos(2*M_PI/365*(day_number+10));
	return declination;
}

/*calculates the hour angle based on location, timezone and current time
 * returns negative angle for time before noon and positive for time after noon*/
float calc_hour_angle_deg(float longitude, int timezone, float hour, float minutes){

	float hra;
	float lt = hour + (minutes / 60.0); /*local time*/
	int lstm = 15 * timezone; /*local standard time meridian*/
	float tc = 4 * (longitude - lstm); /*time correction factor*/
	float lst = lt + (tc / 60.0); /*local solar time*/

	if (lst > 12) {
		lst = lst - 12;
		hra = lst * 15;
	} else {
		hra = -((12 - lst) * 15);
	}
	return hra;
}

/*converts degrees to radians*/
float deg_to_rad(float deg) {
	return deg * M_PI / 180.0;
}

/*calculates elevation based on declination, hra and latitude*/
float calc_elevation(float declination_ang, float hour_ang, float latitude) {

	float arg = sin(deg_to_rad(declination_ang)) * sin(deg_to_rad(latitude)) +
			cos(deg_to_rad(declination_ang)) * cos(deg_to_rad(latitude)) * cos(deg_to_rad(hour_ang));
	float elevation = asinf(arg);
	float elevation_deg = elevation * 180.0 / M_PI;

	return elevation_deg;
}

/*Calculates azimuth based on declination, hra, elevation and latitude*/
float calc_azimuth(float declination_ang, float hour_ang, float latitude, float elevation) {

	float azimuth_deg;
	float numerator = sin(deg_to_rad(declination_ang)) * cos(deg_to_rad(latitude)) -
			cos(deg_to_rad(declination_ang)) * sin(deg_to_rad(latitude)) * cos(deg_to_rad(hour_ang));
	float denominator = cos(deg_to_rad(elevation));
	float azimuth = acosf(numerator / denominator);

	if(hour_ang > 0) {
		azimuth_deg = 360.0 - (azimuth * 180.0 / M_PI);
	} else {
		azimuth_deg = azimuth * 180.0 / M_PI;
	}
return azimuth_deg;
}
/*calculate sun position based on date, time and location*/
void calc_sun_position(RTC_DateTypeDef *date, RTC_TimeTypeDef *time, User_Location *location, Sun_Position *position){

	int day = day_of_year(date);
	float declination = calc_declination_deg(day);
	float hra = calc_hour_angle_deg(location->longitude, location->timezone, time->Hours, time->Minutes);
	float elevation = calc_elevation(declination, hra, location->latitude);
	float azimuth = calc_azimuth(declination, hra, location->latitude, elevation);

	position->azimuth = azimuth;
	position->elevation = elevation;

}
