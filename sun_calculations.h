#ifndef SUN_CALCULATIONS_H
#define SUN_CALCULATIONS_H

extern int day_of_year(RTC_DateTypeDef*);
extern float calc_declination_deg(int);
extern float calc_hour_angle_deg(float, int, float, float);
extern float deg_to_rad(float);
extern float calc_elevation(float, float, float);
extern float calc_azimuth(float, float, float, float);

typedef struct Sun_Position {
	float elevation;
	float azimuth;
} Sun_Position;


typedef struct User_Location {
	float longitude;
	float latitude;
	int timezone;
} User_Location;


void calc_sun_position(RTC_DateTypeDef*, RTC_TimeTypeDef*, User_Location*, Sun_Position*);

#endif
