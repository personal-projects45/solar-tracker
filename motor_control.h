#ifndef MOTOR_CONTROL_H
#define MOTOR_CONTROL_H

#define THETA_90 700
#define THETA_0 1500
#define THETA_NEG_90 2300

extern void stepper_rotate_cw(void);
extern void stepper_rotate_ccw(void);
extern int ang_to_servo(float);

#endif
