/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/

#include "stm32f1xx.h"
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>

#include "motor_control.h"
#include "sun_calculations.h"

#define WRO_LONGITUDE 17
#define WRO_LATITUDE 51.0
#define UTC_1 1

static void systemClock_Config(void);
static void run_once();

TIM_HandleTypeDef tim4;
RTC_HandleTypeDef rtc;
TIM_OC_InitTypeDef oc;

RTC_TimeTypeDef set_time;
RTC_DateTypeDef set_date;

RTC_TimeTypeDef curr_time;
RTC_DateTypeDef curr_date;

GPIO_InitTypeDef gpio;

User_Location location;
Sun_Position curr_position;
Sun_Position next_position;

volatile int motor_position = 0;
volatile int next_motor_position = 0;
volatile int diff;

int main(void)
{

	HAL_Init();
	systemClock_Config();
	HAL_PWR_EnableBkUpAccess();
	__HAL_RCC_BKP_CLK_ENABLE();
	__HAL_RCC_RTC_CONFIG(RCC_RTCCLKSOURCE_LSI);
	__HAL_RCC_RTC_ENABLE();


	gpio.Mode = GPIO_MODE_AF_PP;
	gpio.Pin =  GPIO_PIN_4 | GPIO_PIN_7 | GPIO_PIN_6;
	gpio.Pull = GPIO_NOPULL;
	gpio.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOB, &gpio);

	gpio.Mode = GPIO_MODE_OUTPUT_PP;
	gpio.Pin = GPIO_PIN_6 | GPIO_PIN_7 | GPIO_PIN_8 | GPIO_PIN_9;
	gpio.Pull = GPIO_NOPULL;
	gpio.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOA, &gpio);

	tim4.Instance = TIM4;
	tim4.Init.Period = 20000 - 1;
	tim4.Init.Prescaler = 8 - 1;
	tim4.Init.ClockDivision = 0;
	tim4.Init.CounterMode = TIM_COUNTERMODE_UP;
	tim4.Init.RepetitionCounter  = 0;
	tim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
	HAL_TIM_PWM_Init(&tim4);

	oc.OCMode = TIM_OCMODE_PWM1;
	oc.Pulse = ang_to_servo(0);
	oc.OCPolarity = TIM_OCPOLARITY_HIGH;
	oc.OCNPolarity = TIM_OCNPOLARITY_LOW;
	oc.OCFastMode = TIM_OCFAST_ENABLE;
	oc.OCIdleState = TIM_OCIDLESTATE_SET;
	oc.OCNIdleState = TIM_OCNIDLESTATE_RESET;
	HAL_TIM_PWM_ConfigChannel(&tim4, &oc, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&tim4, TIM_CHANNEL_2);

	rtc.Instance = RTC;
	rtc.Init.AsynchPrediv = RTC_AUTO_1_SECOND;
	rtc.Init.OutPut = RTC_OUTPUTSOURCE_NONE;

	if (HAL_RTC_Init(&rtc) == HAL_OK){

			}
	/*if data in bckup is different than saved then rtc needs reseting*/
	if(HAL_RTCEx_BKUPRead(&rtc, RTC_BKP_DR1) != 0x1212 ) {
		run_once();
	}
	HAL_RTC_GetTime(&rtc, &curr_time, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(&rtc, &curr_date, RTC_FORMAT_BIN);

	while (1)
	 {

		HAL_RTC_GetTime(&rtc, &curr_time, RTC_FORMAT_BIN);
		HAL_RTC_GetDate(&rtc, &curr_date, RTC_FORMAT_BIN);
		oc.Pulse = ang_to_servo(curr_time.Minutes);
		calc_sun_position(&curr_date, &curr_time, &location, &next_position);

		/*setup first servo angle and change when needed*/
		if ((int)next_position.elevation != (int)curr_position.elevation){
			curr_position.elevation = next_position.elevation;
			/*check if working*/
			oc.Pulse = ang_to_servo(curr_time.Minutes);
			HAL_TIM_PWM_ConfigChannel(&tim4, &oc, TIM_CHANNEL_2);
			HAL_TIM_PWM_Start(&tim4, TIM_CHANNEL_2);
		}

		/*setup first position after startup and changed when needed*/
		if ((int)next_position.azimuth != (int)curr_position.azimuth){

			curr_position.azimuth = next_position.azimuth;
			next_motor_position = (int)(next_position.azimuth*1.42);
			diff = next_motor_position-motor_position;
			motor_position = next_motor_position;
		}
		/*rotate motor to desired position*/
		if (diff > 0){
			for(int i = 0; i <= diff; i++){
				stepper_rotate_cw();
			}
			diff = 0;
		} else if (diff < 0){
			for(int i = 0; i >= diff; i--){
				stepper_rotate_ccw();
			}
			diff = 0;
		} /* end if */
	 } /* while(1) */
} /* main */


/*
 * Configure system clocks and oscillators
 */

static void systemClock_Config(void)
{

  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_OscInitTypeDef RCC_OscInitLSE;

  /* Enable Power Control clock */
  __HAL_RCC_PWR_CLK_ENABLE();
  /* Enable GPIO clock and TIM4 clock*/
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_TIM4_CLK_ENABLE();
  __HAL_RCC_RTC_CONFIG(RCC_RTCCLKSOURCE_LSI);
  __HAL_RCC_RTC_ENABLE();
  /* Enable RTC*/
  __HAL_RCC_BKP_CLK_ENABLE();
  __HAL_RCC_RTC_CONFIG(RCC_RTCCLKSOURCE_LSI);
  __HAL_RCC_RTC_ENABLE();
  /*Enable FLASH*/

  /* Enable HSI Oscillator */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  HAL_RCC_OscConfig(&RCC_OscInitStruct);

  /*Enable System clock HSI source and clock dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_SYSCLK_DIV1;
  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2);

  /* Enable LSI oscillator for RTC */
  RCC_OscInitLSE.OscillatorType = RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitLSE.LSIState = RCC_LSI_ON;
  HAL_RCC_OscConfig(&RCC_OscInitLSE);

}
/*this function is meant to run only after reprogramming the uC*/
static void run_once(){
	location.latitude = WRO_LATITUDE;
	location.longitude = WRO_LONGITUDE;

	set_time.Seconds = 00;
	set_time.Hours = 2;
	set_time.Minutes = 0;

	set_date.Month = RTC_MONTH_JANUARY;
	set_date.Date = 14;
	set_date.WeekDay = RTC_WEEKDAY_TUESDAY;
	set_date.Year = 20;

	HAL_RTC_SetTime(&rtc, &set_time, RTC_FORMAT_BIN);
	HAL_RTC_SetDate(&rtc, &set_date, RTC_FORMAT_BIN);
	HAL_RTCEx_BKUPWrite(&rtc, RTC_BKP_DR1, 0x1212);

}
