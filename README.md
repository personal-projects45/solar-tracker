# solar-tracker

This repository holds source files for Solar Tracker project.

Solar Tracker is a STM32 Nucleo board based device that shows real time sun position in the sky.

Tracker derives the elevation and azimuth angles from local time, longitude and timezone.
Most of the calculations are based on those available here:
https://www.pveducation.org/pvcdrom/properties-of-sunlight/the-suns-position

The device uses stepper motor and servo to move the dial around in spherical coordinates. Internal microcontroller's RTC is used to keep track of local time.
